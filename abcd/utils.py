import numpy as np
import sympy


def is_square(mat):
    if not (isinstance(mat, np.ndarray) or isinstance(mat, sympy.MatrixBase)):
        raise TypeError("Expected an np.ndarray or sympy.MatrixBase instance")

    shape = mat.shape
    return shape[0] == shape[1]


def apply_function_to_sympy_matrix(mat: sympy.MatrixBase, f: callable) -> sympy.MatrixBase:
    """
    Apply function f to each element of the matrix, collecting the results in a new matrix of the same shape.
    :param mat: Sympy matrix to apply the function to
    :param f: function taking three parameters: row number, column number, matrix element
    :return: the transformed matrix
    """
    if not isinstance(mat, sympy.MatrixBase):
        raise TypeError("Expected a sympy.MatrixBase instance")
    if not callable(f):
        raise TypeError("f must be callable")

    results = sympy.zeros(*mat.shape)

    for i, row in enumerate(mat.tolist()):
        for j, element in enumerate(row):
            results[i, j] = f(i, j, element)

    return results
