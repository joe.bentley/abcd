import pytest
import numpy as np
import sympy
from . import utils


def test_is_square():
    assert utils.is_square(np.array([[1, 2], [3, 4]]))
    assert not utils.is_square(np.array([[1, 2], [3, 4], [5, 6]]))
    assert utils.is_square(sympy.Matrix([[1, 2], [3, 4]]))
    assert not utils.is_square(sympy.Matrix([[1, 2], [3, 4], [5, 6]]))

    with pytest.raises(TypeError):
        utils.is_square("hello")


def test_apply_function_to_sympy_matrix():
    mat = sympy.Matrix([[1, 2], [3, 4]])
    mat = utils.apply_function_to_sympy_matrix(mat, lambda row, col, x: row + col - x)
    assert mat == sympy.Matrix([[-1, -1], [-2, -2]])
