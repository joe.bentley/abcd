
import numpy as np
import sympy
from numbers import Number
from . import utils
from .inputs import get_wiener_increment


class DimensionError(Exception):
    """
    This class is used to indicate that an error occurred relating to incorrect matrix dimensions
    """
    def __init__(self, message, given=None, expected=None):
        if given is not None and expected is not None:
            message += f" given {given} expected {expected}"
        super(DimensionError, self).__init__(message)

        self.given = given
        self.expected = expected


def _is_expected_system_type(a):
    return isinstance(a, np.ndarray) or isinstance(a, Number) \
           or isinstance(a, sympy.Expr) or isinstance(a, sympy.MatrixBase)


class System:
    """
    This class is for representing state-space models, either numerical (via numpy) or symbolic (via sympy)

    For numerical systems arbitrary inputs can be attached and the system can be simulated in the time-domain,
    but this is not allowed for symbolic systems.

    In abcd symbolic systems are used primarily for finding the physical realizations of systems.
    """
    def __init__(self, a, b, c, d=None, symbolic=False, quantum=False):
        """
        Create a new state-space model.

        A TypeError will be thrown if the parameters are anything except numbers, `np.ndarray`s,
        sympy.Expr, or sympy.MatrixBase

        If any sympy elements are used, self.is_symbolic will be set, in which case set_input and step_evolution
        will be disabled.

        A DimensionError will be thrown for all issues regarding the shapes of a, b, c, d.

        :param a: the internal system dynamics of the system. Must be square.
        :param b: the input-coupling matrix. Must have same number of rows as a and columns as b.
        :param c: the output-coupling matrix. Must have the same number of columns (/rows) as a and rows as d.
        :param d: the direct-feed matrix. Will default to identity if b and c imply num_inputs = num_outputs.
        :param symbolic: force to use symbolic (sympy), even for Numbers, sets self.is_symbolic
        :param quantum: specify that this is a quantum system, sets self.is_quantum
        """
        if not (_is_expected_system_type(a) and
                _is_expected_system_type(b) and
                _is_expected_system_type(c) and
                (_is_expected_system_type(d) or d is None)):
            raise TypeError("a, b, c, d should be instances of np.array or Number")

        # if any of the a, b, c, d are sympy objects, then set this to a symbolic system
        self.is_symbolic = (symbolic or
                            any(map(lambda x: isinstance(x, sympy.Expr)
                                    or isinstance(x, sympy.MatrixBase), [a, b, c, d])))

        self.is_quantum = quantum

        # for a given value, decides whether or not we should wrap it, if yes, wrap in appropriate 1x1 matrix object
        def _wrap(value):
            # don't wrap ndarrays, or sympy Matrices
            # wrap Numbers or sympy Expressions (that aren't also sympy Matrices)
            should_wrap = (not (isinstance(value, sympy.MatrixBase) and not isinstance(value, np.ndarray))
                           and (isinstance(value, Number) or isinstance(value, sympy.Expr)))

            if not should_wrap:
                return value

            value = [[value]]
            return sympy.Matrix(value) if self.is_symbolic else np.array(value)

        a = _wrap(a)
        b = _wrap(b)
        c = _wrap(c)
        d = _wrap(d)

        self.a = a
        self.b = b
        self.c = c

        # if no direct-feed matrix is given, then if number of inputs = number of outputs = n,
        # generate n x n identity, otherwise raise DimensionError
        if d is None:
            if self.num_outputs != self.num_inputs:
                raise DimensionError("If no d matrix is given, need num_inputs = num_outputs "
                                     "i.e. num cols in b = num rows in c")
            d = sympy.Identity(self.num_outputs) if self.is_symbolic else np.identity(self.num_outputs)

        self.d = d
        self.state = np.zeros((self.num_dof, 1))

        # set all inputs to (white-noise) Wiener processes if system is not symbolic
        if not self.is_symbolic:
            self.inputs = []
            self.set_input(get_wiener_increment)
        else:
            self.inputs = None

        # set variable to keep track of current time
        self._time = None

        # check the dimensions of all the matrices
        if not utils.is_square(self.a):
            raise DimensionError("a should be a square matrix")

        if not self.c.shape[1] == self.num_dof:
            raise DimensionError("a and c need the same number of columns to match number of degrees of freedom",
                                 self.c.shape, (self.num_outputs, self.num_dof))

        if not self.d.shape[0] == self.num_outputs:
            raise DimensionError("c and d need the same number of rows to match number of outputs",
                                 self.d.shape, (self.num_outputs, self.num_inputs))

        if not self.b.shape[0] == self.num_dof:
            raise DimensionError("a and b need the same number of rows to match number of degrees of freedom",
                                 self.b.shape, (self.num_dof, self.num_inputs))

        if not self.d.shape[1] == self.num_inputs:
            raise DimensionError("b and d need the same number of columns to match the number of inputs",
                                 self.d.shape, (self.num_dof, self.num_inputs))

    @property
    def num_dof(self):
        """
        The number of degrees of freedom of the system, given by the number of rows (= num columns) of the a matrix.
        For quantum systems the number of degrees of freedom is effectively half this as we consider each pair of
        internal degrees of freedom to be quadrature or sideband pairs.
        :return: the number of rows of the a matrix
        """
        return self.a.shape[0]

    @property
    def num_inputs(self):
        """
        The number of inputs of the system, given by the number of columns of the b matrix.
        For quantum systems, see the caveat at System.num_dof.__doc__
        :return: the number of columns of the b matrix
        """
        return self.b.shape[1]

    @property
    def num_outputs(self):
        """
        The number of inputs of the system, given by the number of rows of the c matrix.
        For quantum systems, see the caveat at System.num_dof.__doc__
        :return: the number of rows of the c matrix
        """
        return self.c.shape[0]

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        """
        Set system state to specified value, which must have shape (self.num_dof, 1) or be a Number.
        If it is a Number then it sets every degree of freedom to that value
        :param value: the new system state
        """
        if isinstance(value, Number):
            value = np.array([[value] * self.num_dof]).T  # extend the value to a (2, 1) ndarray

        if not isinstance(value, np.ndarray):
            raise TypeError("value must be an np.ndarray or a Number")

        expected_shape = (self.num_dof, 1)
        if value.shape != expected_shape:
            raise DimensionError("value did not have the correct shape", value.shape, expected_shape)
        self._state = value

    @property
    def time_elapsed(self):
        return self._time

    def reset_time(self):
        self._time = None

    def set_input(self, new_input_func):
        """
        For _numerical_ systems, set all inputs to a new input function which takes one argument, the timestep dt
        If system is symbolic, raises TypeError.
        :param new_input_func: input function to replace all inputs with
        """
        if self.is_symbolic:
            raise TypeError("Systems with symbolic elements cannot undergo simulation via time evolution")

        self.inputs = [new_input_func for _ in range(self.num_inputs)]

    def step_evolution(self, dt, t=0):
        """
        For _numerical_ systems, perform state evolution for timestep of dt and return output increments.
        If system is symbolic, raises TypeError.
        :param dt: time step of the evolution
        :param t: initial time
        :return: the increment of the output for this step
        """

        if self.is_symbolic:
            raise TypeError("Systems with symbolic elements cannot undergo simulation via time evolution")

        if self._time is None:
            self._time = t

        input_vector = np.array([[input_func(dt, self._time)] for input_func in self.inputs])

        # increment timestep
        self._time += dt

        # state evolution
        dstate = np.matmul(self.a, self.state) * dt + np.matmul(self.b, input_vector)

        self.state = self.state + dstate

        # input-output evolution
        return np.matmul(self.c, self.state) * dt + np.matmul(self.d, input_vector)

    @property
    def j_matrix(self):
        """
        Get the J matrix ("commutation matrix") for the system assuming the degrees of freedom are (self.num_dof / 2)
        quantum sideband operator pairs

        If self.num_dof is not a multiple of two, a DimensionError will be raised.

        :return: The J matrix for the system, used in the physical realizability condition
        """

        if self.num_dof % 2 != 0:
            raise DimensionError("Quantum systems should have 2n internal states (dofs) where n > 0 is integer")

        num_pairs = self.num_dof // 2
        pairs = [1, -1] * num_pairs

        return sympy.diag(*pairs) if self.is_symbolic else np.diag(pairs)

    @property
    def quantum_t_matrix(self):
        """
        Get the Tw matrix for the system assuming _all_ inputs are (self.num_inputs / 2) quantum sideband operator pairs
        with the system having no classical inputs.

        For example, for self.num_inputs (= num b matrix columns) = 2, the input will be assumed to be the pair
        of operators (u, u*)

        If self.num_inputs is not a multiple of two, a DimensionError will be raised.

        :return: The Tw matrix for the system, used in the physical realizability condition
        """

        if self.num_inputs % 2 != 0:
            raise DimensionError("Quantum systems should have 2n input operators where n > 0 is integer")

        num_pairs = self.num_inputs // 2
        pairs = [1, -1] * num_pairs

        return sympy.diag(*pairs) if self.is_symbolic else np.diag(pairs)

    @property
    def is_physically_realizable(self):
        """
        Return true if the system satisfies the physically realizability conditions, using the J and Tw matrices
        given by self.j_matrix, and self.quantum_t_matrix respectively. The conditions are as follows,

        $$
        A J + J A^d + B Tw B^d = 0,
        J C^d + B Tw D^d = 0
        $$

        where ^d denotes Hermitian conjugate

        :return: whether or not the state-space is physically realizable
        """

        j = self.j_matrix
        tw = self.quantum_t_matrix

        if self.is_symbolic:
            assert isinstance(self.a, sympy.MatrixBase)

            cond1 = self.a * j + j * self.a.H + self.b * tw * self.b.H == sympy.zeros(*self.a.shape)
            cond2 = j * self.c.H + self.b * tw * self.d.H == sympy.zeros(*(j * self.c.H).shape)

        else:
            assert isinstance(self.a, np.ndarray)

            cond1 = np.array_equal(np.matmul(self.a, j) + np.matmul(j, self.a.conj().T)
                                   + np.matmul(np.matmul(self.b, tw), self.b.conj().T),
                                   np.zeros(np.matmul(self.a, j).shape))

            cond2 = np.array_equal(np.matmul(j, self.c.conj().T) + np.matmul(np.matmul(self.b, tw), self.d.conj().T),
                                   np.zeros(np.matmul(j, self.c.conj().T).shape))

        return cond1 and cond2

    @property
    def siso_transfer_function(self) -> sympy.Expr:
        """
        Return the transfer function if the system is symbolic and assuming a single-input, single-output (SISO) system

        Will raise DimensionError if system is not SISO
        Will raise TypeError if system is not symbolic

        TODO: handle quantum system with 1-input 1-output, currently only does classical systems
        :return: the calculated transfer function
        """

        if not self.is_symbolic:
            raise TypeError("Symbolic transfer functions can only be computed for symbolic systems")
        if not (self.num_outputs == 1 and self.num_inputs == 1):
            raise DimensionError("This function can only be used on SISO systems")

        s = sympy.symbols('s')
        tf = self.c * (s * sympy.eye(self.num_dof) - self.a)**-1 * self.b + self.d

        # TODO: remove slow simplify
        return sympy.simplify(tf[0])

    @property
    def to_quantum_ss(self):
        """
        Return a new System which is the same but with each row and column "doubled" to accommodate pairs of ladder
        operators. See test_to_quantum_ss in test_system.py for examples

        _For now_ only works for symbolic state spaces. Otherwise will raise TypeError

        :return: a new System with augmented rows and columns
        """

        if not self.is_symbolic:
            raise TypeError("Sorry, augmenting non-symbolic state-spaces not yet implemented")

        def _extend_matrix(mat):
            rows = []
            for i_row in range(mat.shape[0]):
                row = []
                for i_col in range(mat.shape[1]):
                    value = mat[i_row, i_col]
                    row.append(sympy.Matrix([[value, 0], [0, sympy.conjugate(value)]]))
                rows.append(row)
            return sympy.Matrix(sympy.BlockMatrix(rows))

        a_mat = _extend_matrix(self.a)
        b_mat = _extend_matrix(self.b)
        c_mat = _extend_matrix(self.c)
        d_mat = _extend_matrix(self.d)

        sys = System(a_mat, b_mat, c_mat, d_mat)
        sys.is_quantum = True
        return sys

    @property
    def to_physically_realizable(self):
        """
        Transform to the physically realizable state-space. The system must be quantum, set via self.is_quantum
        :return: a physically realizable System
        """
        if not self.is_quantum:
            raise TypeError("Must be a quantum system: self.is_quantum must be True")

        # create matrix of symbols called X[i, j]
        x = sympy.Matrix(sympy.MatrixSymbol('X', self.num_dof, self.num_dof))

        eq1 = self.a * x + x * self.a.adjoint() + self.b * self.j_matrix * self.b.adjoint()

        # for each matrix element solve for X[i, j], giving the x matrix that solves eq1
        x_mat = utils.apply_function_to_sympy_matrix(eq1, lambda i, j, eqn: sympy.solve(eqn, x[i, j]))

        # should also solve the other physical realizability condition
        result_2nd_condition = x_mat * self.c.adjoint() + self.b * self.j_matrix * self.d.adjoint()

        # should be the same TODO: what if it's not true?
        if result_2nd_condition != sympy.zeros(*x.shape):
            raise ValueError("an X matrix that satisfies both physical "
                             "realizability conditions could not be found")

        # check if X Hermitian. Needed so that it can be diagonalized by unitary matrices
        if not x_mat.is_hermitian:
            raise ValueError("resulting X matrix must be Hermitian")

        # we can only perform the correct diagonalization if there is an equal number of positive and negative
        # eigenvalues

        # TODO: the following assumes the values are numbers rather than expressions and symbols
        eigenvals = x_mat.eigenvals()
        positive_evs = list(filter(lambda v: v > 0, eigenvals))
        negative_evs = list(filter(lambda v: v < 0, eigenvals))
        if not len(positive_evs) == len(negative_evs):
            raise ValueError("resulting X matrix must have equal number of positive and negative eigenvalues")

        # X = P D P^\dagger
        p, d = x_mat.diagonalize()

        # we need X in form X = T J T^\dagger, where J is self.j_matrix

        # first we reorganize so that the rows are in the correct order
        positive_rows = []
        negative_rows = []

        for i in range(d.shape[0]):
            eigenval = d[i, i]
            if eigenval > 0:
                positive_rows.append(i)
            if eigenval < 0:
                negative_rows.append(i)

        for i in range(0, d.shape[0], 2):
            if i in negative_rows:
                # swap the negative row with a positive row
                positive_index = positive_rows.pop(0)
                d[i, i], d[positive_index, positive_index] = d[positive_index, positive_index], d[i, i]
                p[i, :], p[positive_index, :] = p[positive_index, :], p[i, :]

        # pull out the eigenvalues into the eigenvector matrix
        t = p * sympy.sqrt(d[0, 0])

        if not t * self.j_matrix * t.adjoint() == x_mat:
            raise ValueError("Error during diagonalization: X != T J T^\\dagger")

        a_mat_prime = t**-1 * self.a * t
        b_mat_prime = t**-1 * self.b
        c_mat_prime = self.c * t
        d_mat_prime = self.d

        sys = System(a_mat_prime, b_mat_prime, c_mat_prime, d_mat_prime, quantum=True)

        if not sys.is_physically_realizable:
            raise ValueError("Resulting system was not physically realizable")

        return sys

    def pprint(self, use_unicode=False):
        """ If symbolic, pretty print all the matrices. """
        if self.is_symbolic:
            # sympy.init_printing()

            print()
            sympy.pprint(self.a, use_unicode=False)
            print()
            sympy.pprint(self.b, use_unicode=False)
            print()
            sympy.pprint(self.c, use_unicode=False)
            print()
            sympy.pprint(self.d, use_unicode=False)
            print()

            # displaying as a state-space equation is not working
            # dx, dt, x, y, u = sympy.symbols('dx dt x y u')
            # Add, Mul, UnevaluatedExpr = sympy.Add, sympy.Mul, sympy.UnevaluatedExpr
            # print(sympy.Eq(dx/dt, UnevaluatedExpr(Add(Mul(self.a, x), Mul(self.b, u)))))
            # print(sympy.Eq(y, UnevaluatedExpr(Add(Mul(self.c, x), Mul(self.d, u)))))
        else:
            raise TypeError("Pretty printing only available for symbolic state spaces")

    @property
    def as_gen_open_osc(self):
        """
        Return the system as a GeneralizedOpenOscillator instance.

        Will raise ValueError if system is not both quantum and symbolic.
        """
        return GeneralizedOpenOscillator(self)


class GeneralizedOpenOscillator:
    """
    Represents the generalized open oscillator G = (S, L, H) for given System (A, B, C, D).

    The system must be quantum and symbolic, and will assumed to be given in terms of ladder operators
    rather than real quadrature operators.
    """
    def __init__(self, system):
        """
        Construct a generalized open oscillator from the given system.

        Will raise ValueError if system is not both quantum and symbolic.
        """

        if not system.is_symbolic:
            raise ValueError("system must be symbolic")
        if not system.is_quantum:
            raise ValueError("system must be quantum")

        # one degree of freedom per each pair of ladder operators
        self.num_oscillators = system.a.shape[0] // 2
        self.system = system

    @property
    def separate(self):
        """
        Separate this n degree of freedom system into n 1 degree of freedom systems
        """
        raise NotImplementedError()

    @property
    def r_matrix(self):
        """
        Calculate the matrix R for the system
        TODO: check that this is correct
        """
        j_mat = self.system.j_matrix
        a_mat = self.system.a
        return sympy.Rational(1, 4) * (-j_mat * a_mat + a_mat.adjoint() * j_mat)

    @property
    def internal_hamiltonian(self):
        """
        Return the internal Hamiltonian, given by H = 1/2 x^d R x where x is the internal system state vector,
        and R is given by self.r_matrix

        Usually zero unless there is detuning or a non-linear process
        """
        return 0

    @property
    def linear_coupling_matrix(self):
        """
        Calculate the linear coupling matrix for the system

        TODO: this isn't gonna work if B isn't square is it??
        """
        return -sympy.Rational(1, 2) * sympy.I * self.system.b.adjoint() * self.system.j_matrix
        # return self.system.j_matrix * self.system.d**-1 * self.system.c

    @property
    def coupling_hamiltonian(self):
        """
        If the system has 1 degree of freedom, write out the coupling Hamiltonian.

        Raises ValueError if system has more than 1 dof (remember 1 dof for each pair of ladder operators)
        You should use #separate() to separate it.
        """
        if self.num_oscillators > 1:
            raise ValueError("generalized open oscillator has more than one internal degree of freedom")

        coupling_matrix = self.linear_coupling_matrix

        a, ad = sympy.symbols('a ad')

        # see network synthesis paper Sec 6.3 "Synthesis of one degree of freedom open oscillators"
        linear_coupling_operator = (coupling_matrix * sympy.Matrix([[a], [ad]]))[0]  # TODO: check this line is correct

        alpha = linear_coupling_operator.coeff(a)
        beta = linear_coupling_operator.coeff(ad)

        gamma_2 = sympy.symbols('gamma_2', real=True, positive=True)

        e2 = (-sympy.sqrt(gamma_2) * alpha).conjugate()
        e1 = sympy.sqrt(gamma_2) * beta

        I = sympy.I
        b, bd = sympy.symbols('b bd')

        return sympy.simplify(I / 2 * (e1 * ad * bd - e1.conjugate() * a * b) +
                              I / 2 * (e2 * ad * b - e2.conjugate() * a * bd))
