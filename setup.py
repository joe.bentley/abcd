from distutils.core import setup

setup(
    name='abcd',
    version='0.1dev',
    packages=['abcd',],
    license='MIT',
    long_description=open('README.md').read(),
)